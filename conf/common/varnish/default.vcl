#
# This is an example VCL file for Varnish.
#
# It does not do anything by default, delegating control to the
# builtin VCL. The builtin VCL is called when there is no explicit
# return statement.
#
# See the VCL chapters in the Users Guide at https://www.varnish-cache.org/docs/
# and http://varnish-cache.org/trac/wiki/VCLExamples for more examples.

# Marker to tell the VCL compiler that this VCL has been adapted to the
# new 4.0 format.
vcl 4.0;
import directors;
import std;
# Default backend definition. Set this to point to your content server.
backend front {
    .host = "127.0.0.1";
    .port = "8080";
    .connect_timeout = 80s;
    .first_byte_timeout = 60s;
    .between_bytes_timeout = 60s;
}

acl purge {
    "localhost";
    "127.0.0.1";
    "::1";
}

sub vcl_init {
  new vdir = directors.round_robin();
  vdir.add_backend(front);
}


sub vcl_recv {

set req.backend_hint = vdir.backend();
set req.http.Host = regsub(req.http.Host, ":[0-9]+", "");
set req.url = std.querysort(req.url);
if (req.method == "PURGE") {
	if (!client.ip ~ purge) {
          return (synth(405, "This IP is not allowed to send PURGE requests."));
        }
#	ban("obj.http.X-eznode == "+regsub(+regsub(req.host, "^/admin\./", ""),":8080", ""));
#      std.syslog(6,"TOTO req.http.host == " + req.http.host + " && req.url ~ " + req.url);      
#	ban("req.http.host == " + req.http.host + " && req.url ~ " + req.url);
        return(synth(200, "Ban added"));
      }
 if (req.url ~ "(ajax\.php|dynamic\.php)") {
    return(pipe);
  }
  # Strip hash, server doesn't need it.
     if (req.url ~ "\#") {
         set req.url = regsub(req.url, "\#.*$", "");
    }
    
    if (req.url ~ "\.(gif|jpg|jpeg|swf|ttf|css|js|flv|mp3|mp4|pdf|ico|png)(\?.*|)$") {
      unset req.http.cookie;
      set req.url = regsub(req.url, "\?.*$", "");
    }

   if (req.url ~ "\?(utm_(campaign|medium|source|term)|adParams|client|cx|eid|fbid|feed|ref(id|src)?|v(er|iew))=") {
     set req.url = regsub(req.url, "\?.*$", "");
   }
# L'accès est interdit sur toutes les urls correspondantes au backoffice de Drupal
   if (req.url ~ "/(index.php|user|admin|users|update|install|flag).*$"){
      return(synth(403, "Not allowed."));
   }

  # Remove google analytics base cookies
   set req.http.Cookie = regsuball(req.http.Cookie, "__utm.=[^;]+(; )?", "");
  set req.http.Cookie = regsuball(req.http.Cookie, "_ga=[^;]+(; )?", "");
  set req.http.Cookie = regsuball(req.http.Cookie, "_gat=[^;]+(; )?", "");
  set req.http.Cookie = regsuball(req.http.Cookie, "utmctr=[^;]+(; )?", "");
  set req.http.Cookie = regsuball(req.http.Cookie, "utmcmd.=[^;]+(; )?", "");
  set req.http.Cookie = regsuball(req.http.Cookie, "utmccn.=[^;]+(; )?", ""); 
  set req.http.Cookie = regsuball(req.http.Cookie, "has_j.=[^;]+(; )?", ""); 
 # Remove a ";" prefix in the cookie if present
    set req.http.Cookie = regsuball(req.http.Cookie, "^;\s*", "");

 if (req.http.cookie ~ "^\s*$") {
    unset req.http.cookie;
  }
  if (req.url ~ "^[^?]*\.(7z|avi|bz2|flac|flv|gz|mka|mkv|mov|mp3|mp4|mpeg|mpg|ogg|ogm|opus|rar|tar|tgz|tbz|txz|wav|webm|xz|zip)(\?.*)?$") {
    unset req.http.Cookie;
    return (hash);
  }


  if (req.http.Accept-Encoding) {
    if (req.http.Accept-Encoding ~ "gzip") {
  	set req.http.Accept-Encoding = "gzip";
    }
    else if (req.http.Accept-Encoding ~ "deflate") {
      set req.http.Accept-Encoding = "deflate";
    }
    else {
      unset req.http.Accept-Encoding;
    }
  }
   unset req.http.Cookie;
  return(hash);
}


# Drop any cookies Wordpress tries to send back to the client.
sub vcl_backend_response {
set beresp.http.X-Url = bereq.url;
set beresp.http.X-Host = bereq.http.host;
unset beresp.http.set-cookie;
    set beresp.ttl = 365d;
  	if (bereq.url ~ "\.(gif|jpg|jpeg|swf|ttf|css|js|flv|mp3|mp4|pdf|ico|png)(\?.*|)$") {
	    set beresp.ttl = 365d;
  	}

}

sub vcl_deliver {
  unset resp.http.Via;
  unset resp.http.X-Whatever;
  unset resp.http.X-Powered-By;
  unset resp.http.X-Varnish;
  unset resp.http.Age;
  unset resp.http.Server;
  unset resp.http.X-Drupal-Cache;
  unset resp.http.x-url;
  unset resp.http.x-host;

   if (obj.hits > 0) {
     set resp.http.X-Cache = "HIT";
   } else {
     set resp.http.X-Cache = "MISS";
   }
   set resp.http.Access-Control-Allow-Origin = "*";
}


sub vcl_hit {
 if (obj.ttl >= 0s) {
    return (deliver);
  }
 return (fetch); # Dead code, keep as a safeguard
}

sub vcl_miss {
  if (req.method == "PURGE") {    
    return(synth(404,"Not cached"));
  }
}
#sub vcl_purge {
#
#}
