<?php

 // Memecached
 $conf['cache_backends'][] = 'sites/all/modules/contrib/memcache_storage/memcache_storage.inc';
# Enable debug mode.
$conf['memcache_storage_debug'] = FALSE;

  // The 'cache_form' bin must be assigned no non-volatile storage.
  $conf['cache_class_cache_form'] = 'DrupalDatabaseCache';
  $conf['cache_class_cache_update'] = 'DrupalDatabaseCache';
  $conf['cache_default_class'] = 'MemcacheStorage';

# Configure memcached extenstion.

  $conf['memcache_storage_key_prefix'] = $prefix;
  $conf['memcache_storage_wildcards_flush_interval'] = 0;
  # Set custom expiration time for cached pages. Together with configured
# Cache Expiration module will relieve the load from your server(s) if you
# have a lot of anonymous users.
$conf['memcache_storage_page_cache_custom_expiration'] = TRUE;
$conf['memcache_storage_page_cache_expire'] = 0;  
$conf['memcache_servers'] = array(
  '127.0.0.1:11211' => 'default',
  '127.0.0.1:11212' => 'session',
  '127.0.0.1:11213' => 'pages',
  '127.0.0.1:11214' => 'bootstrap',
);

  # Store pure HTML for page cache instead of php object.
# Use this config only when you want to reach page cache from nginx or varnish.
$conf['cache_backends'][] = 'sites/all/modules/contrib/memcache_storage/memcache_storage.page_cache.inc';
$conf['cache_class_cache_page'] = 'MemcacheStoragePageCache';
$conf['memcache_storage_external_page_cache'] = TRUE;
$conf['memcache_persistent'] = TRUE;
$conf['memcache_bins'] = array(
  'cache'           => 'default',
  'cache_bootstrap' => 'bootstrap',
  'cache_page'      => 'pages',
  'sessions'      => 'session',
);
# Move storage for lock system into memcached.
$conf['lock_inc'] = 'sites/all/modules/contrib/memcache_storage/includes/lock.inc';
$conf['session_inc'] = 'sites/all/modules/contrib/memcache_storage/includes/session.inc';
